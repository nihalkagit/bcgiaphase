package bcgiaPageObjects;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import bcgiatestcases.BaseClassTest;

public class CreateUserPageObjects extends BaseClassTest
{
	
	WebDriver ldriver;
	public CreateUserPageObjects(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
		
	}
	
	@FindBy(xpath="//*[@id=\'root\']/div/header/div/div[2]/div/a[2]")
	@CacheLookup
	WebElement click;
	@FindBy(xpath="//*[@id=\'root\']/div/div[2]/div[1]/div[1]/div[2]/button")
	@CacheLookup
	WebElement click2;
	@FindBy(xpath="//*[@id=\'root\']/div/div[2]/div[3]/div[2]/form/div[1]/div[1]/div[1]/input")
	@CacheLookup
	WebElement bcgemail;
	
	
	/*
	 * WebElement element=driver.findElement(By.xpath(
	 * "//span[@class='ant-select-selection-item']"));
	 * 
	 * @CacheLookup WebElement businessad;
	 */
	/*
	 * @FindBy(xpath="//div[@class='file-input-upload']")
	 * 
	 * @CacheLookup WebElement uploadImage;
	 */
	
	@FindBy(xpath="//*[@id=\'root\']/div/div[2]/div[3]/div[2]/form/div[1]/div[2]/div[1]/input")
	@CacheLookup
	WebElement bcgFirstname;
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[1]/div[2]/div[2]/input")
	@CacheLookup
	WebElement bcgLastname;
	@FindBy(xpath="//*[@id=\'root\']/div/div[2]/div[3]/div[2]/form/div[1]/div[2]/div[3]/input")
	@CacheLookup
	WebElement bcgCompanyname;
	@FindBy(xpath="//*[@id=\'root\']/div/div[2]/div[3]/div[2]/form/div[1]/div[3]/div[1]/input")
	@CacheLookup
	WebElement userpass;
	@FindBy(xpath="//*[@id=\'root\']/div/div[2]/div[3]/div[2]/form/div[1]/div[3]/div[2]/input")
	@CacheLookup
	WebElement userconfirmPass;
	@FindBy(xpath="//*[@id='root']/div/div[2]/div[3]/div[2]/form/div[3]/div[2]/button[2]")
	WebElement userSubmit;
	
	public void useradd() throws InterruptedException
	{
		click.click();
		click2.click();
		String email = randomString()+"@gmail.com";
		bcgemail.sendKeys(email);
		String name = randomString();
		bcgFirstname.sendKeys(name);
		bcgLastname.sendKeys("Mathew");
		bcgCompanyname.sendKeys("Goodbits");
		//businessad.click();
		userpass.sendKeys("123");
		userconfirmPass.sendKeys("123");
		userSubmit.click();
		
		
		
	}
	

}
