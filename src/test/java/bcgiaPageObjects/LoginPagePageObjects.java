package bcgiaPageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPagePageObjects {
	
	WebDriver ldriver;
	public LoginPagePageObjects(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
		
	}
	
	@FindBy(id="username")
	@CacheLookup
	WebElement Loginusername;
	
	@FindBy(id="password")
	@CacheLookup
	WebElement LoginPassword;
	
	@FindBy(xpath="//input[@type='submit']")
	@CacheLookup
	WebElement Loginbutton;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/header/div/div[1]/div/div[2]/div[2]/button[2]")
	@CacheLookup
	WebElement LogOutbutton;
	
	
	public void setUserName(String uname)
	{
		Loginusername.sendKeys(uname);
	}
	
	public void setPassword(String pword)
	{
		LoginPassword.sendKeys(pword);
	}
	
	public void Login()
	{
		Loginbutton.click();
	}
	public void LogOut()
	{
		LogOutbutton.click();
	}
	
	
}
