package bcgiaPageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserListingPageObjects {

	WebDriver ldriver;
	public UserListingPageObjects(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
		
	}
	
	@FindBy(xpath="//*[@id=\"root\"]/div/header/div/div[2]/div/a[2]")
	@CacheLookup
	WebElement userManageButton;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[1]/div[1]/div[1]/input")
	@CacheLookup
	WebElement userSearch;
	
	
	public void userManageButtonClick()
	{
		userManageButton.click();
	}
	public void userSearch()
	{
		userSearch.sendKeys("Lohith");
	}
	
}
