package bcgiaPageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OverViewPageObjects {
	
	WebDriver ldriver;
	public OverViewPageObjects(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
		
	}
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div/label[1]/span[2]")
	@CacheLookup
	WebElement weekbtn;

	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div/div/input")
	@CacheLookup
	WebElement calbtn;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]/div")
	@CacheLookup
	WebElement selemonthbtn;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div/label[2]/span[2]")
	@CacheLookup
	WebElement selectmonthly;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div/div/input")
	@CacheLookup
	WebElement selectYear;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[2]/div")
	@CacheLookup
	WebElement select2020;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[1]/div/a[2]")
	@CacheLookup
	WebElement userReports;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[2]/div[2]/div[2]/div/div[1]/div[2]/div/div/input")
	@CacheLookup
	WebElement userReportsmonth;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]/div")
	@CacheLookup
	WebElement userReportsmonthsel;
	
	
	
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[1]/div[1]/a[3]")
	@CacheLookup
	WebElement ClientReports;
	
	
	
	
	
	public void setWeek()
	{
		weekbtn.click();
	}
	
	public void setCal()
	{
		calbtn.click();
	}
	
	public void setMonth()
	{
		selemonthbtn.click();
	}
	
	public void selectMonthly()
	{
		selectmonthly.click();
	}
	
	public void selectYear()
	{
		selectYear.click();
	}
	
	public void selectYear2020()
	{
		select2020.click();
	}
	
	public void selectUserReport()
	{
		userReports.click();
	}
	public void selectUserReportMonth()
	{
		userReportsmonth.click();
	}
	
	public void selectUserReportMonthJan()
	{
		userReportsmonthsel.click();
	}
	
	public void ClientReportsbtn()
	{
		ClientReports.click();
	}
	
}


