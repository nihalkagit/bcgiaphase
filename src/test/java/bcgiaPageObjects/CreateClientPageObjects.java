package bcgiaPageObjects;

//import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import bcgiatestcases.BaseClassTest;

public class CreateClientPageObjects extends BaseClassTest
{

	WebDriver ldriver;
	public CreateClientPageObjects(WebDriver rdriver)
	{
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
		
	}
	
	@FindBy(xpath="//*[@id=\"root\"]/div/header/div/div[2]/div/a[3]")
	@CacheLookup
	WebElement cClientbtn;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[1]/div[1]/div[2]/button")
	@CacheLookup
	WebElement Clientaddbtn;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[1]/div[1]/div[1]/input")
	@CacheLookup
	WebElement Clientemail;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[1]/div[2]/div[1]/input")
	@CacheLookup
	WebElement Clientfname;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[1]/div[2]/div[2]/input")
	@CacheLookup
	WebElement ClientLname;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[1]/div[2]/div[3]/input")
	@CacheLookup
	WebElement Clientcompname;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[1]/div[3]/div[1]/input")
	@CacheLookup
	WebElement Clientpass1;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[1]/div[3]/div[2]/input")
	@CacheLookup
	WebElement Clientpass2;
	
	@FindBy(xpath="//*[@id=\"root\"]/div/div[2]/div[3]/div[2]/form/div[3]/button[2]")
	@CacheLookup
	WebElement CreateBtn;
	
	public void createClient()
	{
		
		cClientbtn.click();
		Clientaddbtn.click();
		String email1 = randomString()+"@gmail.com";
		Clientemail.sendKeys(email1);
		Clientfname.sendKeys("Arjun");
		ClientLname.sendKeys("Menon");
		Clientcompname.sendKeys("GoodBits");
		Clientpass1.sendKeys("123");
		Clientpass2.sendKeys("123");
		CreateBtn.click();
		
		
		
	}
	
	
}
