package bcgiatestcases;



import bcgiaPageObjects.CreateUserPageObjects;
import bcgiaPageObjects.LoginPagePageObjects;
import org.testng.annotations.Test;

public class TC_Useradd_003Test extends BaseClassTest
{	
	
	
	@Test
	public void addu() throws InterruptedException
	{	
		logger.info("TestCase_002 Started Executing");
		LoginPagePageObjects log = new LoginPagePageObjects(driver);
		log.setUserName(uname);
		logger.info("Username entered");
		log.setPassword(pword);
		logger.info("Password entered");
		log.Login();
		logger.info("Loggedin Successfully");
		
		Thread.sleep(3000);
		
		CreateUserPageObjects cu = new CreateUserPageObjects(driver);
		cu.useradd();
		logger.info("User Added Successfully");
		//log.LogOut();
		logger.info("TestCase_002 Execution ended");
	}
}
