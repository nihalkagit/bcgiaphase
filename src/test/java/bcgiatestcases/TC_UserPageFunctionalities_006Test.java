package bcgiatestcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import bcgiaPageObjects.LoginPagePageObjects;
import bcgiaPageObjects.UserListingPageObjects;

public class TC_UserPageFunctionalities_006Test extends BaseClassTest
{

	@Test
	public void UserPageFunct() throws InterruptedException, IOException
	{
		logger.info("Started TestCase_005 Execution");
		LoginPagePageObjects log1 = new LoginPagePageObjects(driver);
		log1.setUserName(uname);
		logger.info("Username entered");
		log1.setPassword(pword);
		logger.info("Password entered");
		log1.Login();
		logger.info("Login successfully");
		UserListingPageObjects ul = new UserListingPageObjects(driver);
		ul.userManageButtonClick();
		logger.info("Clicked the User Management Button");
		Thread.sleep(3000);
		ul.userSearch();
		
		boolean res = driver.getPageSource().contains("Lohith");
		if(res== true)
		{
			Assert.assertTrue(true);
			logger.info("Search Functionality is working perfectly");
		}
		else
		{
			captureScreen(driver,"UserPageFunct");
			Assert.assertTrue(false);
			logger.warn("Search Functionality is Failed");
		}
		Thread.sleep(3000);
		
		logger.info("Ended TestCase_005 Execution");
	}
}
