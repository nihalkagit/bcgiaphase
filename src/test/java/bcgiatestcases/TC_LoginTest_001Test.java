package bcgiatestcases;

import org.testng.annotations.Test;

import java.io.IOException;

import org.testng.Assert;

import bcgiaPageObjects.LoginPagePageObjects;

public class TC_LoginTest_001Test extends BaseClassTest 
{
	@Test
	public void loginTest() throws IOException
	{
		logger.info("Testcase_001 started executing");
		LoginPagePageObjects lp = new LoginPagePageObjects(driver);
		lp.setUserName(uname);
		logger.info("username enterd");
		lp.setPassword(pword);
		logger.info("password enterd");
		lp.Login();
		
		if(driver.getTitle().equals("BCG IA"))
		{
			Assert.assertTrue(true);
			logger.info("Successfully LoggedIn");
		}
		else
		{
			captureScreen(driver,"loginTest");
			Assert.assertTrue(false);
			logger.info("Login failed");
			
		}
		logger.info("Testcase_001 execution ended");
	}

}
