package bcgiatestcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import bcgiaPageObjects.CreateClientPageObjects;
//import bcgiaPageObjects.CreateUserPageObjects;
import bcgiaPageObjects.LoginPagePageObjects;

public class TC_CreateClient_004Test extends BaseClassTest 
{
	@Test
	public void clientAdd() throws InterruptedException, IOException
	{
		logger.info("TestCase_003 Started Executing");
		LoginPagePageObjects log1 = new LoginPagePageObjects(driver);
		log1.setUserName(uname);
		logger.info("Username entered");
		log1.setPassword(pword);
		logger.info("Password entered");
		log1.Login();
		logger.info("Login successfully");
		
		CreateClientPageObjects cup = new CreateClientPageObjects(driver);
		cup.createClient();
		Thread.sleep(3000);
		boolean res = driver.getPageSource().contains("Client added successfully!");
		if(res== true)
		{
			Assert.assertTrue(true);
			logger.info("Successfully created");
		}
		else
		{
			captureScreen(driver,"clientAdd");
			Assert.assertTrue(false);
			logger.warn("Failed");
		}
		logger.info("Ended the TestCase_003 Execution");
	}

}
