package bcgiatestcases;

import java.io.IOException;

import org.testng.annotations.Test;

import bcgiaPageObjects.LoginPagePageObjects;
import bcgiaPageObjects.OverViewPageObjects;

public class TC_OverviewClickingFunctionality_005Test extends BaseClassTest
{
	@Test
	public void OverviewData() throws InterruptedException, IOException
	{
		logger.info("TestCase_004 Started Executing");
		LoginPagePageObjects log1 = new LoginPagePageObjects(driver);
		log1.setUserName(uname);
		logger.info("Username entered");
		log1.setPassword(pword);
		logger.info("Password Enterd");
		log1.Login();
		logger.info("Logged in Successfully");
		
		OverViewPageObjects ov = new OverViewPageObjects(driver);
		ov.setWeek();
		logger.info("Clicked on the week");
		ov.setCal();
		logger.info("Clicked on the Calendar");
		ov.setMonth();
		logger.info("Month selected");
		Thread.sleep(3000);
		ov.selectMonthly();
		logger.info("Clicked on the monthly button");
		ov.selectYear();
		logger.info("Clicked on the year selection button");
		
		ov.selectYear2020();
		logger.info("Selected the year as 2020");
		ov.selectUserReport();
		logger.info("Clicked on the User report tab button");
		ov.selectUserReportMonth();
		logger.info("Selected the month button in the user reports page");
		ov.selectUserReportMonthJan();
		logger.info("Selected Jan as the month");
		ov.ClientReportsbtn();
		logger.info("Clcked on the Client report button");
		Thread.sleep(3000);
		
		logger.info("Ended the testcase_004 execution");
		
		
	}
}
	
