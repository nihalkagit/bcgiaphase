package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
	
	Properties pro;
	
	public ReadConfig()
	{
		File src = new File("/home/ubuntu/new/bcgiaphase/Configuration/config.properties");
		try {
			FileInputStream fis = new FileInputStream(src);
			pro = new Properties() ;
			pro.load(fis);
			
		}
		catch (Exception e)
		{
			System.out.println("exception is :" + e.getMessage());
		}
	}
	
	public String getApplicationURL()
	{
		String url = pro.getProperty("baseURL");
		return url;
	}
	
	public String getUsername()
	{
		String username = pro.getProperty("uname");
		return username;
	}
	
	public String getPassword()
	{
		String password = pro.getProperty("pword");
		return password;
	}
	
	public String getChromepath()
	{
		String chromePath = pro.getProperty("chrome_path");
		return chromePath;
	}
	
	public String getIepath()
	{
		String ie = pro.getProperty("iepath");
		return ie;
	}
	
	public String getFirefoxpath()
	{
		String firefox = pro.getProperty("firefoxpath");
		return firefox;
	}
}
